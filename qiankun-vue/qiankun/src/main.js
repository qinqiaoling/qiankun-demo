import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

let instance = null
function render(props) {
  const { container } = props;
  console.log('父调子传参数', props)
  console.log('子应用container', container)
  instance = new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount(container ? container.querySelector('#app') : '#app')// 这里是挂载到自己的html中  基座会拿到这个挂载后的html 将其插入进去
}

if (window.__POWERED_BY_QIANKUN__) {
  // (判断是自启动还是父应用调用)动态添加publicPath,主要解决的是微应用动态载入的 脚本、样式、图片 等地址不正确的问题
  __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__;
}

if (!window.__POWERED_BY_QIANKUN__) {
  // 默认独立运行
  render()
}

// 父应用记载子应用，子应用必须暴露三个接口：bootstrap、mount、unmount
// 子组件的协议就OK了
/**
 * bootstrap 只会在微应用初始化的时候调用一次，下次微应用重新进入时会直接调用 mount 钩子，不会再重复触发 bootstrap。
 * 通常我们可以在这里做一些全局变量的初始化，比如不会在 unmount 阶段被销毁的应用级别的缓存等。
 */
export async function bootstrap(props) { }
/**
 * 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法
 */
export async function mount(props) {
  console.log('进入子应用', props)
  console.log('store', store, store.state)
  // 使用 Vue 原型属性
  Vue.prototype.parentStore = props
  // 监听全局状态，子应用更新主应用数据后触发
  props.onGlobalStateChange((state) => {
    console.log('子应用接受的主应用数据')
    console.log(state)
    // store.state = Object.assign({}, store.state, JSON.parse(JSON.stringify(state)))
    store.replaceState(Object.assign({}, store.state, state))
  }, true);
  render(props)
}
/**
 * 应用每次 切出/卸载 会调用的方法，通常在这里我们会卸载微应用的应用实例
 */
export async function unmount(props) {
  instance.$destroy();//页面卸载
}

// new Vue({
//   router,
//   store,
//   render: h => h(App)
// }).$mount('#app')
