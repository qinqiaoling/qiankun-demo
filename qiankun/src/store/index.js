import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
    num_change_fun: (state, data) => {
      state.num = data
    },
    from_zhu_change_fun: (state, data) => {
      state.num1 = data
    },
    // 主应用激活菜单
    zhu_menuactive_fun: (state, data) => {
      state.zhu_menuactive = data
    },
    // 收集子传主的数据
    child_to_zhu_fun: (state, data) => {
      console.log('child_to_zhu_fun---', data)
      state.lang = data.lang
      state.name = data.name
    },
    // 清除数据
    rest_clear(state) {
      state.num = 0
      state.lang = ''
      state.name = ''
    }
  },
  actions: {
    child_to_zhu_fun(context, data) {
      context.commit('child_to_zhu_fun', data)
    },
  },
  modules: {

  },
  getters: {

  }
})
