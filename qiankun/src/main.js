import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

Vue.config.productionTip = false

console.log('process.env', process.env.NODE_ENV)
// 保存整站vuex本地储存初始状态
import cloneDeep from 'lodash/cloneDeep'
// window.SITE_CONFIG['storeState'] = cloneDeep(store.state)

import { addGlobalUncaughtErrorHandler, registerMicroApps, start } from 'qiankun';

registerMicroApps([
  // {
  //   name: 'react app', // app name registered
  //   entry: '//localhost:7100',
  //   container: '#yourContainer',
  //   activeRule: '/yourActiveRule',
  // },
  {
    name: 'vue-app',//应用的名字
    // entry: { scripts: ['//localhost:8080/main.js'] },//默认会加载这个html解析里面的js动态的执行(子应用必须支持跨域)fetch
    entry: process.env.NODE_ENV == 'development' ? '//localhost:8079' : '/child/dist',//默认会加载这个html解析里面的js动态的执行(子应用必须支持跨域)fetch
    container: '#vue',//容器名，（此项目页面中定义的容器id，用于把对面的子应用放到此容器中）
    activeRule: '/vue',//激活的路径
    props: { a: store },//传递的值（可选）
  },
], {
  // qiankun 生命周期钩子 - 微应用加载前
  beforeLoad: (app) => {
    // 加载微应用前，加载进度条
    console.log('before load =====', app.name)
    return Promise.resolve()
  },
  // qiankun 生命周期钩子 - 微应用挂载后
  afterMount: (app) => {
    // 加载微应用前，进度条加载完成
    console.log('after mount=====', app.name)
    return Promise.resolve()
  }
});

//  添加全局的未捕获异常处理器
addGlobalUncaughtErrorHandler((event) => {
  console.error(event)
  const { message: msg } = event
  if (msg && msg.includes('died in status LOADING_SOURCE_CODE')) {
    console.error('微应用加载失败，请检查应用是否可运行')
  }
})

start();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


